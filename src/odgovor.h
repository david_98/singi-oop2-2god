#ifndef ODGOVOR_H_
#define ODGOVOR_H_

#include <SDL2/SDL_ttf.h>
#include <string>
#include "drawable.h"

using namespace std;

class Odgovor: public Drawable {
private:
	SDL_Renderer *renderer;
	TTF_Font *font;
	SDL_Color color;
	SDL_Surface *textSurface;
	SDL_Texture *text;
	SDL_Rect textRect;
public:
	Odgovor(SDL_Renderer*, int, int, int, int, int);
	SDL_Texture* getTexture();
	SDL_Rect& getRectangle();
	virtual void draw(SDL_Renderer*);
	void setX(int);
	int getX();
	virtual ~Odgovor();
};

#endif /* ODGOVOR_H_ */
