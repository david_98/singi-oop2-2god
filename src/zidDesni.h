#ifndef ZIDDESNI_H_
#define ZIDDESNI_H_

#include <iostream>
#include "tileset.h"

#include "zid.h"

using namespace std;

class ZidDesni : public Zid {
private:
//	Tileset* tileset;
//	ZidMatrica zid;
	// pocetne koordinate zida, mnoze se jos sa width u draw metodi
	double x = 512;
	double y = 160;
public:
	ZidDesni(istream &inputStream, Tileset *tileset);
	virtual void draw(SDL_Renderer * renderer);
	void setX(double);
	void setY(double);
	double getX();
	double getY();
	virtual ~ZidDesni();
};

#endif /* ZIDDESNI_H_ */
