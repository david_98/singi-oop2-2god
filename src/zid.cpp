/*
 * zid.cpp
 *
 *  Created on: Jan 31, 2020
 *      Author: david
 */

#include "zid.h"

Zid::Zid(istream &inputStream, Tileset *tileset): Drawable() {
	this->tileset = tileset;
    int h, w;
    string tileCode;
    inputStream >> h >> w;
    for(int i = 0; i < h; i++) {
        zid.push_back(ZidRed());
        for(int j = 0; j < w; j++) {
            inputStream >> tileCode;
            zid[i].push_back(tileCode);
        }
    }
}

Zid::~Zid() {
	delete tileset;
}

