#include <iostream>
#include <fstream>

#include "engine.h"
#include "level.h"
#include "sprite.h"
#include "zid.h"
#include "zidDesni.h"
#include "zidLevi.h"

using namespace std;


int main(int argc, char** argv)
{
    Engine *eng = new Engine("RPG Game");

    eng->addTileset("resources/tilesets/mathgame_podloga_tileset.txt", "podloga");

    ifstream podlogaStream("resources/levels/mathgame_podloga_level.txt");
	eng->addDrawable(new Level(podlogaStream, eng->getTileset("podloga")));

    eng->run(); /** Pokretanje glavne petlje igre. */
    delete eng; /** Oslobadjanje memorije koju je zauzela instanca Engine klase. */
    return 0;
}
