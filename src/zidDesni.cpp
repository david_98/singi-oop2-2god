/*
 * zidDesni.cpp
 *
 *  Created on: Jan 31, 2020
 *      Author: david
 */

#include "zidDesni.h"

ZidDesni::ZidDesni(istream &inputStream, Tileset *tileset): Zid(inputStream, tileset) {

}

void ZidDesni::draw(SDL_Renderer * renderer){
	double x2 = x;
	double y2 = y;
    for(size_t i = 0; i < zid.size(); i++) {
        for(size_t j = 0; j < zid[i].size(); j++) {
        	if (zid[i][j] != "n"){
        		tileset->drawTile(zid[i][j], x2, y2, renderer);
        	}

            x2 += 32;
        }
        y2 += 32;
        x2 = x;
    }
}

void ZidDesni::setX(double br){
	this->x = br;
}
double ZidDesni::getX(){
	return x;
}
void ZidDesni::setY(double br){
	this->y = br;
}
double ZidDesni::getY(){
	return y;
}

ZidDesni::~ZidDesni() {
}

