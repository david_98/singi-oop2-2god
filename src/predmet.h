
#ifndef PREDMET_H_
#define PREDMET_H_

#include <string>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "drawable.h"
#include "kolizijaInterface.h"
#include "tile.h"

//#include "spritesheet.h"

using namespace std;

class Predmet: public Drawable, public KolizijaInterface {
private:
//	SpriteSheet *sheet;
	SDL_Rect *dest;
	SDL_Texture *texture;
	Tile* tile;
public:
	Predmet(istream &inputStream, SDL_Renderer *renderer, int x, int y, int width, int height);
    virtual void draw(SDL_Renderer *renderer);
    void drawObject(SDL_Renderer* renderer);
    virtual int getX();
    virtual int getY();
    virtual int getW();
    virtual int getH();

	virtual ~Predmet();
};

#endif /* PREDMET_H_ */
