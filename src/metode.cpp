/*
 * metode.cpp
 *
 *  Created on: Jan 30, 2020
 *      Author: david
 */

#include "metode.h"

Metode::Metode() {
	// TODO Auto-generated constructor stub

}

Metode::~Metode() {
	// TODO Auto-generated destructor stub
}

bool Metode::kolizijaTester(KolizijaInterface* igrac, KolizijaInterface* predmet){
//	int getAPredmet = predmet->getW()/2;
//	int getBPredmet = predmet->getH()/2;
//	float poluprecnikPredmet = sqrt(getAPredmet*getAPredmet + getBPredmet*getBPredmet);

	float poluprecnikPredmet = predmet->getW()/2;

//	int getAPlayer = igrac->getW()/2;
//	int getBPlayer = igrac->getH()/2;
//	float poluprecnikPlayer = sqrt(getAPlayer*getAPlayer + getBPlayer*getBPlayer);

	float poluprecnikPlayer = igrac->getW()/2 - 22;

	float dozvljenoRastojanje = poluprecnikPredmet + poluprecnikPlayer;


	float igracX = igrac->getX() + igrac->getW()/2;
	float predmetX = predmet->getX() + predmet->getW()/2;
	float x = abs(igracX - predmetX);

	float igracY = igrac->getY() + igrac->getH()/4*3;
	float predmetY = predmet->getY() + predmet->getH()/2;
	float y = abs(igracY - predmetY);

	float stvarnoRastojanje = sqrt(x*x + y*y);


	if (stvarnoRastojanje <= dozvljenoRastojanje){
		return true;
	};
	return false;
}
