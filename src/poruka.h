#ifndef PORUKA_H_
#define PORUKA_H_

#include <SDL2/SDL_ttf.h>
#include <string>
#include "drawable.h"

using namespace std;

class Poruka: public Drawable {
private:
	SDL_Renderer *renderer;
	TTF_Font *font;
	SDL_Color color;
	SDL_Surface *textSurface;
	SDL_Texture *text;
	SDL_Rect textRect;
public:
	Poruka(SDL_Renderer*, string, int, int, int, int);
	virtual void draw(SDL_Renderer*);
	virtual ~Poruka();
};

#endif /* PORUKA_H_ */
