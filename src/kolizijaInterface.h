/*
 * kolizijaInterface.h
 *
 *  Created on: Jan 30, 2020
 *      Author: david
 */

#ifndef KOLIZIJAINTERFACE_H_
#define KOLIZIJAINTERFACE_H_

class KolizijaInterface {
public:
	KolizijaInterface();
	virtual ~KolizijaInterface();
	virtual int getX()=0;
	virtual int getY()=0;
	virtual int getW()=0;
	virtual int getH()=0;
};

#endif /* KOLIZIJAINTERFACE_H_ */
