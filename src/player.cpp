#include "player.h"

Player::Player(Sprite *sprite) : Drawable(), Movable(), KeyboardEventListener() {
    this->sprite = sprite;
}

void Player::draw(SDL_Renderer *renderer) {
    sprite->draw(renderer);
}

void Player::move() {
    sprite->move();
}

void Player::move(int dx, int dy) {
    sprite->move(dx, dy);
}

void Player::listen(SDL_KeyboardEvent &event) {
	if(event.type == SDL_KEYDOWN) {
        if(event.keysym.sym == SDLK_LEFT) {
//        	if(sprite->getSpriteRect()->x == 0){
//        		sprite->setState(Sprite::STOP);
//        	} else {
			sprite->setState(Sprite::LEFT);
//        	}

        } else if(event.keysym.sym == SDLK_RIGHT) {
            sprite->setState(Sprite::RIGHT);
        } else if(event.keysym.sym == SDLK_UP) {
            sprite->setState(Sprite::UP);
        } else if(event.keysym.sym == SDLK_DOWN) {
            sprite->setState(Sprite::DOWN);
        }
    } else if (event.type == SDL_KEYUP) {
        if(event.keysym.sym == SDLK_LEFT) {
            sprite->setState(Sprite::LEFT_STOP);
        } else if(event.keysym.sym == SDLK_RIGHT) {
            sprite->setState(Sprite::RIGHT_STOP);
        } else if(event.keysym.sym == SDLK_UP) {
            sprite->setState(Sprite::UP_STOP);
        } else if(event.keysym.sym == SDLK_DOWN) {
            sprite->setState(Sprite::DOWN_STOP);
        }
    }

}

Sprite* Player::getSprite(){
	return sprite;
}
