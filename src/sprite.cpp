#include "sprite.h"

Sprite::Sprite(SpriteSheet *sheet, int width, int height, ZidDesni* zidDesni, ZidLevi* zidLevi, Engine* eng) : Drawable(), Movable(), KolizijaInterface() {
    state = Sprite::UP_STOP;
    this->sheet = sheet;
    currentFrame = 0;
    frameCounter = 0;
    frameSkip = 1;

    spriteRect = new SDL_Rect();
    spriteRect->x = 0;
    spriteRect->y = 0;
    spriteRect->w = width;
    spriteRect->h = height;

	this->zidDesni = zidDesni;
	this->zidLevi = zidLevi;
//	this->prikazaniZadatak = prikazaniZadatak;
    this->eng = eng;
}

int Sprite::getFrameSkip() {
    return frameSkip;
}

void Sprite::setFrameSkip(int frameSkip) {
    if(frameSkip < 0) {
        frameSkip = 0;
    }

    this->frameSkip = frameSkip+1;
}

short int Sprite::getState() {
    return state;
}

void Sprite::setState(short int state) {
    this->state = state;
}

void Sprite::draw(SDL_Renderer *renderer) {
    if(state == LEFT) {
        sheet->drawFrame("walk_left", currentFrame, spriteRect, renderer);
    } else if(state == RIGHT) {
        sheet->drawFrame("walk_right", currentFrame, spriteRect, renderer);
    } else if(state == UP) {
        sheet->drawFrame("walk_up", currentFrame, spriteRect, renderer);
    } else if(state == DOWN) {
        sheet->drawFrame("walk_down", currentFrame, spriteRect, renderer);
    } else if(state == LEFT_STOP) {
        sheet->drawFrame("walk_left", 0, spriteRect, renderer);
    } else if(state == RIGHT_STOP) {
        sheet->drawFrame("walk_right", 0, spriteRect, renderer);
    } else if(state == UP_STOP) {
        sheet->drawFrame("walk_up", 0, spriteRect, renderer);
    } else if(state == DOWN_STOP) {
        sheet->drawFrame("walk_down", 0, spriteRect, renderer);
    }

    frameCounter++;
    if(frameCounter%frameSkip == 0) {
        currentFrame++;
        if(currentFrame >= 9) {
            currentFrame = 0;
        }
        frameCounter = 0;
    }
}

void Sprite::move(int dx, int dy) {
    spriteRect->x += dx;
    spriteRect->y += dy;

	for (Predmet* predmet: predmeti){
		if (Metode::kolizijaTester(this, predmet)){
			// desila se kolizija
			// za DRUGI NACIN prikaza zadataka spomenut u engine.cpp
//			eng->setPogledaoZadatak();
			if (state == 1){
				state = LEFT_STOP;
				spriteRect->x -= dx;
				spriteRect->y -= dy;
			} else if (state == 2){
				state = RIGHT_STOP;
				spriteRect->x -= dx;
				spriteRect->y -= dy;
			} else if (state == 4){
				state = UP_STOP;
				spriteRect->x -= dx;
				spriteRect->y -= dy;
			} else if (state == 8){
				state = DOWN_STOP;
				spriteRect->x -= dx;
				spriteRect->y -= dy;
			}
			cout << "kolizija" << endl;
			break;
		};
	}
	// za desni zid provera, kad se igrac krece
	if (spriteRect->x >= zidDesni->getX()-40){
		if (spriteRect->y > 180 && spriteRect->y < 205){
			if (spriteRect->x >= zidDesni->getX()-20){
				if (eng->getZadatak()->getPozicijaOdgovora() == 4){

					eng->setNivo(eng->getNivo()+1);
					spriteRect->x = zidLevi->getX()+280;
					spriteRect->y = 192;

				// netacan odgovor
				} else {
					if (eng->getNivo() != 0){
						eng->setNivo(eng->getNivo()-1);
					};
					spriteRect->x = zidLevi->getX()+280;
					spriteRect->y = 192;

				}

			}
		} else if (spriteRect->y > 308 && spriteRect->y < 333) {
			if (spriteRect->x >= zidDesni->getX()){
				if (eng->getZadatak()->getPozicijaOdgovora() == 5){

					eng->setNivo(eng->getNivo()+1);
					spriteRect->x = zidLevi->getX()+280;
					spriteRect->y = 320;

					// netacan odgovor
				} else {
					if (eng->getNivo() != 0){
						eng->setNivo(eng->getNivo()-1);
					};
					spriteRect->x = zidLevi->getX()+280;
					spriteRect->y = 320;

				}
			}
		} else if (spriteRect->y > 436 && spriteRect->y < 461){
			if (spriteRect->x >= zidDesni->getX()){
				if (eng->getZadatak()->getPozicijaOdgovora() == 6){

					eng->setNivo(eng->getNivo()+1);
					spriteRect->x = zidLevi->getX()+280;
					spriteRect->y = 448;

					// netacan odgovor
				} else {
					if (eng->getNivo() != 0){
						eng->setNivo(eng->getNivo()-1);
					};
					spriteRect->x = zidLevi->getX()+280;
					spriteRect->y = 448;

				}
			}
		} else {
			move(-1, 0);
		}
	}



	// za levi zid
	if (spriteRect->x <= zidLevi->getX()+294){
		if (spriteRect->y > 180 && spriteRect->y < 205){
			if (spriteRect->x <= zidLevi->getX()+274){
				if (eng->getZadatak()->getPozicijaOdgovora() == 1){

					eng->setNivo(eng->getNivo()+1);
					spriteRect->x = zidDesni->getX()-30;
					spriteRect->y = 192;

				// netacan odgovor
				} else {
					if (eng->getNivo() != 0){
						eng->setNivo(eng->getNivo()-1);
					};
					spriteRect->x = zidDesni->getX()-30;
					spriteRect->y = 192;

				}

			}
		} else if (spriteRect->y > 308 && spriteRect->y < 333) {
			if (spriteRect->x <= zidLevi->getX()+274){
				if (eng->getZadatak()->getPozicijaOdgovora() == 2){

					eng->setNivo(eng->getNivo()+1);
					spriteRect->x = zidDesni->getX()-30;
					spriteRect->y = 320;

					// netacan odgovor
				} else {
					if (eng->getNivo() != 0){
						eng->setNivo(eng->getNivo()-1);
					};
					spriteRect->x = zidDesni->getX()-30;
					spriteRect->y = 320;

				}
			}
		} else if (spriteRect->y > 436 && spriteRect->y < 461){
			if (spriteRect->x <= zidLevi->getX()+274){
				if (eng->getZadatak()->getPozicijaOdgovora() == 3){

					eng->setNivo(eng->getNivo()+1);
					spriteRect->x = zidDesni->getX()-30;
					spriteRect->y = 448;

					// netacan odgovor
				} else {
					if (eng->getNivo() != 0){
						eng->setNivo(eng->getNivo()-1);
					};
					spriteRect->x = zidDesni->getX()-30;
					spriteRect->y = 448;

				}
			}
		} else {
			move(+1, 0);
		}
	}

}

void Sprite::move() {
	if(state == 1) {
		move(-1, 0);
	}
	if(state == 2) {
		move(1, 0);
	}
	if(state == 4) {
		// ogranicenje za kretanje ka gore
		if (spriteRect->y == 120){
			state = UP_STOP;
		} else {
			move(0, -1);
		}
	}
	if(state == 8) {
		// ogranicenje za kretanje ka dole
		if (spriteRect->y == 444){
			state = DOWN_STOP;
		} else {
			move(0, 1);
		}

	}
}




void Sprite::putInPredmeti(Predmet* predmet){
	predmeti.push_back(predmet);
}

SDL_Rect* Sprite::getSpriteRect(){
	return spriteRect;
}


int Sprite::getX(){
	return spriteRect->x;
}
int Sprite::getY(){
	return spriteRect->y;
}
int Sprite::getW(){
	return spriteRect->w;
}
int Sprite::getH(){
	return spriteRect->h;
}
