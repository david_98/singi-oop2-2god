#ifndef ZIDLEVI_H_
#define ZIDLEVI_H_

#include <iostream>
#include "tileset.h"

#include "zid.h"

using namespace std;

class ZidLevi : public Zid {
private:
//	Tileset* tileset;
//	ZidMatrica zid;
	// pocetne koordinate zida
	double x = -192;
	double y =160;
public:
	ZidLevi(istream &inputStream, Tileset *tileset);
	virtual void draw(SDL_Renderer * renderer);
	void setX(double);
	void setY(double);
	double getX();
	double getY();
	virtual ~ZidLevi();
};

#endif /* ZIDLEVI_H_ */
