#include "poruka.h"

Poruka::Poruka(SDL_Renderer* renderer, string tekst, int x, int y, int w, int h): Drawable() {
	this->renderer = renderer;
	this->font = TTF_OpenFont("lazy.ttf", 30);
	this->color = {255, 255, 255, 255};
	this->textSurface = TTF_RenderText_Solid(font, tekst.c_str(), color);
	this->text = SDL_CreateTextureFromSurface(renderer, textSurface);
	SDL_FreeSurface(textSurface);
	this->textRect.x = x;
	this->textRect.y = y;
	this->textRect.w = w;
	this->textRect.h = h;

	SDL_QueryTexture(text, NULL, NULL, &textRect.w, &textRect.h);

}
void Poruka::draw(SDL_Renderer* renderer){
	SDL_RenderCopy(renderer, text, NULL, &textRect);
}

Poruka::~Poruka() {
	// TODO Auto-generated destructor stub
}

