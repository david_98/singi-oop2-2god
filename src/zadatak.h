#ifndef ZADATAK_H_
#define ZADATAK_H_

#include <SDL2/SDL_ttf.h>
#include <string>
#include <vector>
#include "drawable.h"

using namespace std;

class Zadatak: public Drawable {
private:
	int x, y1, y2;
	vector<int> rezultatiZadatka;
	int pozicijaOdgovora;
	SDL_Renderer *renderer;
	TTF_Font *font;
	SDL_Color color;
	SDL_Surface *textSurface;
	SDL_Texture *text;
	SDL_Rect textRect;
public:
	Zadatak(SDL_Renderer*, string, vector<int>, int);
	vector<int> getRezultatiZadatka();
	int getPozicijaOdgovora();
	SDL_Texture* getTexture();
	SDL_Rect& getRectangle();
	virtual void draw(SDL_Renderer*);
	virtual ~Zadatak();
};

#endif /* ZADATAK_H_ */
