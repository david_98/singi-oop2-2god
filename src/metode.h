#ifndef METODE_H_
#define METODE_H_

#include <cmath>
#include <cstdlib>

#include "kolizijaInterface.h"

class Metode {
public:
	Metode();
	static bool kolizijaTester(KolizijaInterface*, KolizijaInterface*);
	virtual ~Metode();
};

#endif /* METODE_H_ */
