/*
 * odgovor.cpp
 *
 *  Created on: Feb 1, 2020
 *      Author: david
 */

#include "odgovor.h"

Odgovor::Odgovor(SDL_Renderer* renderer, int broj, int x, int y, int w, int h) {
	this->renderer = renderer;
	this->font = TTF_OpenFont("lazy.ttf", 20);
	this->color = {255, 255, 255, 255};
	this->textSurface = TTF_RenderText_Solid(font, std::to_string(broj).c_str(), color);
	this->text = SDL_CreateTextureFromSurface(renderer, textSurface);
	SDL_FreeSurface(textSurface);
	this->textRect.x = x;
	this->textRect.y = y;
	this->textRect.w = w;
	this->textRect.h = h;

	SDL_QueryTexture(text, NULL, NULL, &textRect.w, &textRect.h);


}

SDL_Texture* Odgovor::getTexture(){
	return text;
}

SDL_Rect& Odgovor::getRectangle(){
	return textRect;
}

void Odgovor::setX(int kordinata){
	this->textRect.x = kordinata;
}

int Odgovor::getX(){
	return textRect.x;
}

void Odgovor::draw(SDL_Renderer* renderer){
	SDL_RenderCopy(renderer, text, NULL, &textRect);
}

Odgovor::~Odgovor() {
	// TODO Auto-generated destructor stub
}

