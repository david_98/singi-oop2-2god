#include "engine.h"

ostream& operator<<(ostream& out, const Level& l) {
    int rows = l.getLevelMatrix().size();
    int cols = 0;
    if(rows > 0) {
        cols = l.getLevelMatrix()[0].size();
    }
    out << rows << " " << cols << endl;

    for(int i = 0; i < rows; i++){
        for(int j = 0; j < cols; j++) {
            out << l.getLevelMatrix()[i][j] << " ";
        }
        out << endl;
    }

    return out;
}

Engine::Engine(string title) {
    SDL_Init(SDL_INIT_VIDEO);
    IMG_Init(IMG_INIT_PNG);
    window = SDL_CreateWindow(title.c_str(), SDL_WINDOWPOS_UNDEFINED,
                              SDL_WINDOWPOS_UNDEFINED, 640, 605, SDL_WINDOW_RESIZABLE);
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

}

void Engine::addTileset(Tileset *tileset, const string &name) {
    tilesets[name] = tileset;
}

void Engine::addTileset(istream &inputStream, const string &name) {
    addTileset(new Tileset(inputStream, renderer), name);
}

void Engine::addTileset(const string &path, const string &name) {
    ifstream tilesetStream(path);
    addTileset(tilesetStream, name);
}

Tileset* Engine::getTileset(const string &name) {
    return tilesets[name];
}

void Engine::addDrawable(Drawable *drawable) {
    drawables.push_back(drawable);
}

void Engine::setZidDesni(ZidDesni* zidDesni){
	this->zidDesni = zidDesni;
}
void Engine::setZidLevi(ZidLevi* zidLevi){
	this->zidLevi = zidLevi;
}
ZidDesni* Engine::getZidDesni(){
	return zidDesni;
}
ZidLevi* Engine::getZidLevi(){
	return zidLevi;
}

Zadatak* Engine::getZadatak(){
	return this->prikazaniZadatak;
}

void Engine::setNivo(int noviNivo){
	this->nivo = noviNivo;
}
int Engine::getNivo(){
	return nivo;
}

void Engine::setPogledaoZadatak(){
	pogledaoZadatak = true;
}

void Engine::run() {
    int maxDelay = 1000/frameCap;
    int frameStart = 0;
    int frameEnd = 0;

    bool running = true;
    SDL_Event event;

    cout << (*dynamic_cast<Level*>(drawables[0])) << endl;

    // za zidove
    // desni zid
    ifstream zidDesniStream("resources/objects/zidDesni.txt");
	ZidDesni* zidDesni = new ZidDesni(zidDesniStream, this->getTileset("podloga"));

	// levi zid
    ifstream zidLeviStream("resources/objects/zidLevi.txt");
	ZidLevi* zidLevi = new ZidLevi(zidLeviStream, this->getTileset("podloga"));

//	this->addDrawable(zidDesni);
//	this->addDrawable(zidLevi);


    // za predmete
    // dzak
    ifstream predmet01Stream("resources/objects/predmet01.txt");
    Predmet *predmet01 = new Predmet(predmet01Stream, renderer, 315, 255, 32, 32);

    drawables.push_back(predmet01);

    // postansko sanduce
    ifstream predmet02Stream("resources/objects/predmet02.txt");
	Predmet *predmet02 = new Predmet(predmet02Stream, renderer, 315, 225, 32, 32);

	drawables.push_back(predmet02);


    // za playera
    ifstream spriteSheetStream("resources/creatures/sprite_sheet.txt");
    SpriteSheet *sheet = new SpriteSheet(spriteSheetStream, renderer);

    Sprite *sp = new Sprite(sheet, 64, 64, zidDesni, zidLevi, this);
    sp->setFrameSkip(4);

    sp->putInPredmeti(predmet01);
    sp->putInPredmeti(predmet02);

    Player *player = new Player(sp);
    player->getSprite()->getSpriteRect()->x = 285;
    player->getSprite()->getSpriteRect()->y = 292;


	drawables.push_back(player);
    movables.push_back(player);
    eventListeners.push_back(player);

    // zidovi
	this->addDrawable(zidDesni);
	this->addDrawable(zidLevi);

	// za crtanje teksta
	if (TTF_Init() < 0){
		cout << "Error" << TTF_GetError() << endl;
	}
	// kreiranje zadataka i ubacivanje u zadaci mapu
	// zadaci za prvi nivo
	Zadatak* zadatak01 = new Zadatak(renderer, "20+8+6", {34, 33, 32, 35, 36, 37}, 1);
	Zadatak* zadatak02 = new Zadatak(renderer, "22+22+12", {56, 54, 52, 66, 22, 12}, 1);
	Zadatak* zadatak03 = new Zadatak(renderer, "33+8+6", {46, 45, 48, 47, 54, 56}, 4);
	Zadatak* zadatak04 = new Zadatak(renderer, "24+18+6", {47, 52, 40, 43, 58, 48}, 6);
	Zadatak* zadatak05 = new Zadatak(renderer, "20+8+16", {45, 46, 43, 44, 55, 54}, 4);
	Zadatak* zadatak06 = new Zadatak(renderer, "7+8+14", {29, 32, 27, 28, 30, 31}, 1);

	vector<Zadatak*> zadaci1 = {zadatak01, zadatak02, zadatak03, zadatak04, zadatak05, zadatak06};
	zadaci.insert({0, zadaci1});

	// zadaci za drugi nivo
	Zadatak* zadatak11 = new Zadatak(renderer, "66-33-12", {21, 22, 23, 31, 32, 33}, 1);
	Zadatak* zadatak12 = new Zadatak(renderer, "68-24-12", {33, 42, 43, 36, 32, 35}, 5);
	Zadatak* zadatak13 = new Zadatak(renderer, "66-7-45", {15, 14, 25, 11, 7, 16}, 2);
	Zadatak* zadatak14 = new Zadatak(renderer, "56-24-5", {27, 22, 28, 26, 29, 17}, 1);
	Zadatak* zadatak15 = new Zadatak(renderer, "44-11-12", {22, 20, 32, 31, 26, 21}, 6);
	Zadatak* zadatak16 = new Zadatak(renderer, "66-2-48", {15, 14, 24, 25, 16, 26}, 5);

	vector<Zadatak*> zadaci2 = {zadatak11, zadatak12, zadatak13, zadatak14, zadatak15, zadatak16};
	zadaci.insert({1, zadaci2});

	// zadaci za treci nivo
	Zadatak* zadatak21 = new Zadatak(renderer, "12*2*4", {44, 74, 94, 76, 96, 86}, 5);
	Zadatak* zadatak22 = new Zadatak(renderer, "2*4*8", {44, 64, 74, 62, 80, 32}, 2);
	Zadatak* zadatak23 = new Zadatak(renderer, "56*2*3", {255, 366, 240, 436, 236, 336}, 6);
	Zadatak* zadatak24 = new Zadatak(renderer, "12*7*1", {84, 88, 80, 78, 70, 77}, 1);
	Zadatak* zadatak25 = new Zadatak(renderer, "45*2*2", {120, 188, 180, 160, 170, 185}, 3);
	Zadatak* zadatak26 = new Zadatak(renderer, "4*4*4", {52, 66, 32, 62, 64, 65}, 5);

	vector<Zadatak*> zadaci3 = {zadatak21, zadatak22, zadatak23, zadatak24, zadatak25, zadatak26};
	zadaci.insert({2, zadaci3});

	// zadaci za cetvrti nivo
	Zadatak* zadatak31 = new Zadatak(renderer, "12/2/3", {2, 4, 3, 5, 6, 1}, 1);
	Zadatak* zadatak32 = new Zadatak(renderer, "48/4/3", {5, 6, 4, 7, 9, 3}, 3);
	Zadatak* zadatak33 = new Zadatak(renderer, "56/8/7", {12, 8, 2, 1, 3, 4}, 4);
	Zadatak* zadatak34 = new Zadatak(renderer, "40/1/40", {20, 40, 1, 10, 80, 0}, 3);
	Zadatak* zadatak35 = new Zadatak(renderer, "45/9/5", {5, 6, 1, 4, 2, 12}, 3);
	Zadatak* zadatak36 = new Zadatak(renderer, "28/7/2", {2, 4, 8, 16, 7, 12}, 1);

	vector<Zadatak*> zadaci4 = {zadatak31, zadatak32, zadatak33, zadatak34, zadatak35, zadatak36};
	zadaci.insert({3, zadaci4});

	// zadaci za peti nivo
	Zadatak* zadatak41 = new Zadatak(renderer, "12*2+3", {24, 26, 27, 28, 29, 25}, 3);
	Zadatak* zadatak42 = new Zadatak(renderer, "48*4+16", {208, 212, 206, 122, 202, 208}, 1);
	Zadatak* zadatak43 = new Zadatak(renderer, "12*2+60", {85, 82, 62, 64, 74, 84}, 6);
	Zadatak* zadatak44 = new Zadatak(renderer, "6*5*4", {60, 240, 220, 125, 120, 140}, 5);
	Zadatak* zadatak45 = new Zadatak(renderer, "30+4*4", {120, 132, 134, 46, 138, 140}, 4);
	Zadatak* zadatak46 = new Zadatak(renderer, "7*4+2", {32, 30, 24, 34, 38, 28}, 2);

	vector<Zadatak*> zadaci5 = {zadatak41, zadatak42, zadatak43, zadatak44, zadatak45, zadatak46};
	zadaci.insert({4, zadaci5});

	// zadaci za sesti nivo
	Zadatak* zadatak51 = new Zadatak(renderer, "36/6*2", {24, 12, 10, 16, 15, 14}, 2);
	Zadatak* zadatak52 = new Zadatak(renderer, "36/6+2", {16, 4, 206, 8, 12, 6}, 4);
	Zadatak* zadatak53 = new Zadatak(renderer, "102/6*2", {7, 24, 17, 34, 40, 44}, 4);
	Zadatak* zadatak54 = new Zadatak(renderer, "102/6+2", {8, 9, 11, 29, 17, 19}, 6);
	Zadatak* zadatak55 = new Zadatak(renderer, "5*6-12", {18, 28, 32, 12, 26, 24}, 1);
	Zadatak* zadatak56 = new Zadatak(renderer, "5+6*12", {77, 34, 86, 80, 82, 86}, 1);

	vector<Zadatak*> zadaci6 = {zadatak51, zadatak52, zadatak53, zadatak54, zadatak55, zadatak56};
	zadaci.insert({5, zadaci6});

    while(running) {
        frameStart = SDL_GetTicks();
        // uslov za prekid igre, ako razmak izmedju dva zida iznosi manje od 64
        if (zidDesni->getX() < 340 && !presaoSve){
        	Poruka* poruka1 = new Poruka(renderer, "Izgubili ste.", 200, 200, 192, 32);
        	zavrsioIgricu = true;
        	drawables.push_back(poruka1);
        };
        if (zidDesni->getX() < 320){
        	break;
        }
        while(SDL_PollEvent(&event)) {
            if(event.type == SDL_QUIT) {
                running = false;
            } else {
            	// sa ovim spritovi namestaju postavljaju svoj state
                for(size_t i = 0; i < eventListeners.size(); i++) {
                    eventListeners[i]->listen(event);
                }
            }
        }
        // sad ti spritovi izvravaju move() spram svojih state-ova koje su iznad sa listen() usvojili
        if (!zavrsioIgricu && nivo != 6){
        	for(size_t i = 0; i < movables.size(); i++) {
        		movables[i]->move();
        	}
        }

        // postavlja boju za crtanje
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
        // crta sa tom bojom u pozadini
        SDL_RenderClear(renderer);


        // radimo biranje zadatka i postavljanje odgovora
        // OVAJ NACIN ne daje mogucnost da se zadatak i moguca resenja prikazu tek kad dodje do kolizije igraca sa kamenom ili sanducetom
        // ispod njega je nacin koji bi trebao da to omoguci ali mi izbacuje beli ekran i gasi ga, nisam uspeo da pronadjem gresku pa sam ostavio ovaj nacin
        if (trenutniNivo != nivo){
        	trenutniNivo = nivo;
        	// za kraj igre
        	if (trenutniNivo == 6){
        		presaoSve = true;
        		zavrsioIgricu = true;
        		Poruka* poruka2 = new Poruka(renderer, "Uspeli ste.", 200, 200, 192, 32);
        		drawables.push_back(poruka2);
        	};
        	// uklanjanje vec poostavljenog zadatka, 6 odgovora i bundeva ako su dodate u drawables
        	if (!prvoPokretanje && (trenutniNivo != 6)){
        		if (prosliNivo == 0){
        			// znaci da smo u drawables dodali 1 bundevu, zadatak, 6 odgovora
        			for (int i = 0; i < 8; i++){
        				drawables.pop_back();
        			}
        		} else if (prosliNivo == 1){
        			// imamo zadatak, 6 odgovora i 2 bundeve
        			for (int i = 0; i < 9; i++){
						drawables.pop_back();
					}
        		} else if (prosliNivo == 2){
        			for (int i = 0; i < 10; i++){
						drawables.pop_back();
					}
        		} else if (prosliNivo == 3){
        			for (int i = 0; i < 11; i++){
						drawables.pop_back();
					}
        		} else if (prosliNivo == 4){
        			for (int i = 0; i < 12; i++){
						drawables.pop_back();
					}
        		} else if (prosliNivo == 5){
        			for (int i = 0; i < 13; i++){
						drawables.pop_back();
					}
        		}
        	}
        	if (trenutniNivo != 6){

        		// kreiranje bundeva za prikaz trenutnog nivoa
				int pocetnoX = 191;
				int i = 0;
				for (; i <= nivo; i++){
					pocetnoX += 32;
					ifstream predmet03Stream("resources/objects/predmet03.txt");
					drawables.push_back(new Predmet(predmet03Stream, renderer, pocetnoX, 544, 32, 32));

				}

				srand((unsigned) time(0));
				int randomNumber;
				randomNumber = rand() % 6;
				vector<Zadatak*> trazeniVektor = zadaci.at(nivo);
				prikazaniZadatak = trazeniVektor[randomNumber];
				odgovor1 = new Odgovor(renderer, prikazaniZadatak->getRezultatiZadatka()[0], 20, 215, 32, 32);
				odgovor2 = new Odgovor(renderer, prikazaniZadatak->getRezultatiZadatka()[1], 20, 345, 32, 32);
				odgovor3 = new Odgovor(renderer, prikazaniZadatak->getRezultatiZadatka()[2], 20, 468, 32, 32);
				odgovor4 = new Odgovor(renderer, prikazaniZadatak->getRezultatiZadatka()[3], 600, 215, 32, 32);
				odgovor5 = new Odgovor(renderer, prikazaniZadatak->getRezultatiZadatka()[4], 600, 345, 32, 32);
				odgovor6 = new Odgovor(renderer, prikazaniZadatak->getRezultatiZadatka()[5], 600, 468, 32, 32);


				drawables.push_back(prikazaniZadatak);
				drawables.push_back(odgovor1);
				drawables.push_back(odgovor2);
				drawables.push_back(odgovor3);
				drawables.push_back(odgovor4);
				drawables.push_back(odgovor5);
				drawables.push_back(odgovor6);

        	}


        };

        // OVAJ NACIN bi trebao da omoguci prikaz zadatka i odgovora tek kad dodje do kolizije sa kamenom i sanducetom ali javlja gore pomenuti problem
        /*

        if (trenutniNivo != nivo){
        	trenutniNivo = nivo;
        	// pogledaoZadatak menjam na true u sprite.cpp kad dodje do kolizije
        	pogledaoZadatak = false;
        	// neophodan kako se ne bi generisali ponovo zadaci dok smo na istom levelu
        	uslovZaPrikaz = true;
        	// za kraj igre
        	if (trenutniNivo == 6){
        		break;
        	};
        	// uklanjanje vec poostavljenog zadatka, 6 odgovora i bundeva ako su dodate u drawables
        	if (!prvoPokretanje){
        		if (prosliNivo == 0){
        			// znaci da smo u drawables dodali 1 bundevu, zadatak, 6 odgovora
        			for (int i = 0; i < 8; i++){
        				drawables.pop_back();
        			}
        		} else if (prosliNivo == 1){
        			// imamo zadatak, 6 odgovora i 2 bundeve
        			for (int i = 0; i < 9; i++){
						drawables.pop_back();
					}
        		} else if (prosliNivo == 2){
        			for (int i = 0; i < 10; i++){
						drawables.pop_back();
					}
        		} else if (prosliNivo == 3){
        			for (int i = 0; i < 11; i++){
						drawables.pop_back();
					}
        		} else if (prosliNivo == 4){
        			for (int i = 0; i < 12; i++){
						drawables.pop_back();
					}
        		} else if (prosliNivo == 5){
        			for (int i = 0; i < 13; i++){
						drawables.pop_back();
					}
        		}
        	}

        	// kreiranje bundeva za prikaz trenutnog nivoa
        	int pocetnoX = 191;
        	int i = 0;
        	for (; i <= nivo; i++){
        		pocetnoX += 32;
        		ifstream predmet03Stream("resources/objects/predmet03.txt");
				drawables.push_back(new Predmet(predmet03Stream, renderer, pocetnoX, 544, 32, 32));

        	}

        }

		if (pogledaoZadatak && uslovZaPrikaz) {
			srand((unsigned) time(0));
			int randomNumber;
		    randomNumber = rand() % 6;
        	vector<Zadatak*> trazeniVektor = zadaci.at(nivo);
        	prikazaniZadatak = trazeniVektor[randomNumber];
        	odgovor1 = new Odgovor(renderer, prikazaniZadatak->getRezultatiZadatka()[0], 20, 215, 32, 32);
        	odgovor2 = new Odgovor(renderer, prikazaniZadatak->getRezultatiZadatka()[1], 20, 345, 32, 32);
        	odgovor3 = new Odgovor(renderer, prikazaniZadatak->getRezultatiZadatka()[2], 20, 468, 32, 32);
        	odgovor4 = new Odgovor(renderer, prikazaniZadatak->getRezultatiZadatka()[3], 600, 215, 32, 32);
        	odgovor5 = new Odgovor(renderer, prikazaniZadatak->getRezultatiZadatka()[4], 600, 345, 32, 32);
        	odgovor6 = new Odgovor(renderer, prikazaniZadatak->getRezultatiZadatka()[5], 600, 468, 32, 32);



        	cout << drawables.size() << endl;

        	drawables.push_back(prikazaniZadatak);
        	drawables.push_back(odgovor1);
        	drawables.push_back(odgovor2);
        	drawables.push_back(odgovor3);
        	drawables.push_back(odgovor4);
        	drawables.push_back(odgovor5);
        	drawables.push_back(odgovor6);

        	// neophodan kako se ne bi generisali ponovo zadaci dok smo na istom levelu
        	uslovZaPrikaz = false;
        };

        */


        // postavljanje x kordinate za pomeranje zidova
        zidDesni->setX(zidDesni->getX() - 0.04);
        zidLevi->setX(zidLevi->getX() + 0.04);

        for(size_t i = 0; i < drawables.size(); i++) {
            drawables[i]->draw(renderer);
        }

        prvoPokretanje = false;
        prosliNivo = nivo;


        if (!zavrsioIgricu){

			// za desni zid provera, kad igrac miruje
			if (sp->getSpriteRect()->x >= zidDesni->getX()-40){
				if (sp->getSpriteRect()->y > 180 && sp->getSpriteRect()->y < 205){
					if (sp->getSpriteRect()->x > zidDesni->getX()-20){
						if (prikazaniZadatak->getPozicijaOdgovora() == 4){

							nivo = nivo + 1;
							sp->getSpriteRect()->x = zidLevi->getX()+280;
							sp->getSpriteRect()->y = 192;

						// netacan odgovor
						} else {
							if (nivo != 0){
								nivo = nivo - 1;
							};
							sp->getSpriteRect()->x = zidLevi->getX()+280;
							sp->getSpriteRect()->y = 192;

						}

					}
				} else if (sp->getSpriteRect()->y > 308 && sp->getSpriteRect()->y < 333){
					if (sp->getSpriteRect()->x >= zidDesni->getX()){
						if (prikazaniZadatak->getPozicijaOdgovora() == 5){

							nivo = nivo + 1;
							sp->getSpriteRect()->x = zidLevi->getX()+280;
							sp->getSpriteRect()->y = 320;

						// netacan odgovor
						} else {
							if (nivo != 0){
								nivo = nivo - 1;
							};
							sp->getSpriteRect()->x = zidLevi->getX()+280;
							sp->getSpriteRect()->y = 320;

						}

					}
				} else if (sp->getSpriteRect()->y > 436 && sp->getSpriteRect()->y < 461){
					if (sp->getSpriteRect()->x >= zidDesni->getX()){
						if (prikazaniZadatak->getPozicijaOdgovora() == 6){

							nivo = nivo + 1;
							sp->getSpriteRect()->x = zidLevi->getX()+280;
							sp->getSpriteRect()->y = 448;

						// netacan odgovor
						} else {
							if (nivo != 0){
								nivo = nivo - 1;
							};
							sp->getSpriteRect()->x = zidLevi->getX()+280;
							sp->getSpriteRect()->y = 448;

						}

					}
				} else {
					sp->move(-1, 0);
				}
			}
			// za levi zid provera kad miruje
			if (sp->getSpriteRect()->x <= zidLevi->getX()+294){
				if (sp->getSpriteRect()->y > 180 && sp->getSpriteRect()->y < 205){
					if (sp->getSpriteRect()->x <= zidLevi->getX()+274){
						if (prikazaniZadatak->getPozicijaOdgovora() == 1){

							nivo = nivo + 1;
							sp->getSpriteRect()->x = zidDesni->getX()-30;
							sp->getSpriteRect()->y = 192;

						// netacan odgovor
						} else {
							if (nivo != 0){
								nivo = nivo - 1;
							};
							sp->getSpriteRect()->x = zidDesni->getX()-30;
							sp->getSpriteRect()->y = 192;

						}

					}
				} else if (sp->getSpriteRect()->y > 308 && sp->getSpriteRect()->y < 333){
					if (sp->getSpriteRect()->x <= zidLevi->getX()+274){
						if (prikazaniZadatak->getPozicijaOdgovora() == 2){

							nivo = nivo + 1;
							sp->getSpriteRect()->x = zidDesni->getX()-30;
							sp->getSpriteRect()->y = 320;

						// netacan odgovor
						} else {
							if (nivo != 0){
								nivo = nivo - 1;
							};
							sp->getSpriteRect()->x = zidDesni->getX()-30;
							sp->getSpriteRect()->y = 320;

						}

					}
				} else if (sp->getSpriteRect()->y > 436 && sp->getSpriteRect()->y < 461){
					if (sp->getSpriteRect()->x <= zidLevi->getX()+274){
						if (prikazaniZadatak->getPozicijaOdgovora() == 3){

							nivo = nivo + 1;
							sp->getSpriteRect()->x = zidDesni->getX()-30;
							sp->getSpriteRect()->y = 448;

						// netacan odgovor
						} else {
							if (nivo != 0){
								nivo = nivo - 1;
							};
							sp->getSpriteRect()->x = zidDesni->getX()-30;
							sp->getSpriteRect()->y = 448;

						}

					}
				} else {
					sp->move(+1, 0);
				}
			}
    	}

        SDL_RenderPresent(renderer);

        frameEnd = SDL_GetTicks();
        if(frameEnd - frameStart < maxDelay) {
            SDL_Delay(maxDelay - (frameEnd - frameStart));
        }

     }
}

Engine::~Engine() {
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
//    IMG_Quit();
    SDL_Quit();
}
