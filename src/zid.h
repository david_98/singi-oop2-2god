#ifndef ZID_H_
#define ZID_H_

#include <iostream>
#include <vector>

#include "drawable.h"
#include "tileset.h"

using namespace std;

class Zid : public Drawable {
public:
	typedef vector<vector<string> > ZidMatrica;
	typedef vector<string> ZidRed;
protected:
	Tileset* tileset;
	ZidMatrica zid;
public:
	Zid(istream &inputStream, Tileset *tileset);

	virtual void draw(SDL_Renderer * renderer)=0;

	virtual ~Zid();
};

#endif /* ZID_H_ */
