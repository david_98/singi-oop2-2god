#ifndef ENGINE_H_INCLUDED
#define ENGINE_H_INCLUDED

#include <fstream>
#include <string>
#include <vector>
#include <map>

#include <iostream>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include <cstdlib>
#include <ctime>
#include "drawable.h"
#include "movable.h"
#include "tileset.h"
#include "level.h"
#include "spritesheet.h"
#include "sprite.h"
#include "predmet.h"
#include "zid.h"
#include "zidDesni.h"
#include "zidLevi.h"
#include "zadatak.h"
#include "odgovor.h"
#include "poruka.h"

#include "eventlistener.h"

#include "player.h"

using namespace std;

class Engine {
private:
    map<string, Tileset*> tilesets;
    vector<Drawable*> drawables;
    vector<Movable*> movables;
    vector<EventListener*> eventListeners;
    SDL_Window *window;
    SDL_Renderer *renderer;
    int frameCap = 60;
    ZidDesni* zidDesni;
    ZidLevi* zidLevi;
    // za tekst zadatka
    map<int, vector<Zadatak*> > zadaci;
    int nivo = 0;
    int trenutniNivo = -1;
    int prosliNivo;
    Zadatak* prikazaniZadatak;
//    SDL_Rect zadatakPoz;
    Odgovor* odgovor1;
    Odgovor* odgovor2;
    Odgovor* odgovor3;
    Odgovor* odgovor4;
    Odgovor* odgovor5;
    Odgovor* odgovor6;
    bool prvoPokretanje = true;
    bool pogledaoZadatak;
    bool uslovZaPrikaz;
    bool presaoSve = false;
    bool zavrsioIgricu = false;
public:

    Engine(string title);

    void addTileset(Tileset *tileset, const string &name);

    void addTileset(istream &inputStream, const string &name);

    void addTileset(const string &path, const string &name);

    Tileset* getTileset(const string &name);

    Zadatak* getZadatak();

    void addDrawable(Drawable* drawable);

    void setZidDesni(ZidDesni*);
    void setZidLevi(ZidLevi*);
    ZidDesni* getZidDesni();
    ZidLevi* getZidLevi();
    void setPogledaoZadatak();

    int getNivo();
    void setNivo(int);

    void run();

    virtual ~Engine();
};

#endif // ENGINE_H_INCLUDED
