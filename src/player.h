#ifndef PLAYER_H_INCLUDED
#define PLAYER_H_INCLUDED

#include "drawable.h"
#include "keyboardeventlistener.h"
#include "movable.h"


class Sprite;
#include "sprite.h"

class Player: public Drawable, public Movable, public KeyboardEventListener {
private:
    Sprite *sprite;
public:
    Player(Sprite* sprite);
    virtual void draw(SDL_Renderer *renderer);
    virtual void move();
    virtual void move(int dx, int dy);
    virtual void listen(SDL_KeyboardEvent &event);
    Sprite* getSprite();

};

#endif // PLAYER_H_INCLUDED
