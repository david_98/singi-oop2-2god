/*
 * zidLevi.cpp
 *
 *  Created on: Jan 31, 2020
 *      Author: david
 */

#include "zidLevi.h"

ZidLevi::ZidLevi(istream &inputStream, Tileset *tileset): Zid(inputStream, tileset) {

}

void ZidLevi::draw(SDL_Renderer * renderer){
	// ovde odredjujemo gde se pocinje sa iscrtavanjem levog zida
	double x2 = x;
	double y2 = y;
    for(size_t i = 0; i < zid.size(); i++) {
        for(size_t j = 0; j < zid[i].size(); j++) {
        	if (zid[i][j] != "n"){
        		tileset->drawTile(zid[i][j], x2, y2, renderer);
        	}

            x2 += 32;
        }
        y2 += 32;
        x2 = x;
    }
}

void ZidLevi::setX(double br){
	this->x = br;
}
double ZidLevi::getX(){
	return x;
}
void ZidLevi::setY(double br){
	this->y = br;
}
double ZidLevi::getY(){
	return y;
}

ZidLevi::~ZidLevi() {
	// TODO Auto-generated destructor stub
}

