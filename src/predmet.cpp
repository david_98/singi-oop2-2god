/*
 * predmet.cpp
 *
 *  Created on: Jan 30, 2020
 *      Author: david
 */

#include "predmet.h"

Predmet::Predmet(istream &inputStream, SDL_Renderer* renderer, int x, int y, int width, int height): Drawable(), KolizijaInterface(){
	string path;
	inputStream >> path;

	SDL_Surface *surface = IMG_Load(path.c_str());
	texture = SDL_CreateTextureFromSurface(renderer, surface);
	SDL_FreeSurface(surface);

	tile = new Tile(inputStream);

	dest = new SDL_Rect();
    dest->x = x;
    dest->y = y;
    dest->w = width;
    dest->h = height;
}

void Predmet::draw(SDL_Renderer* renderer){
	drawObject(renderer);
}

void Predmet::drawObject(SDL_Renderer* renderer){
	SDL_RenderCopy(renderer, texture, tile->getRect(), dest);
}
Predmet::~Predmet() {
//	SDL_DestroyTexture(texture);
}
int Predmet::getX(){
	return dest->x;
}
int Predmet::getY(){
	return dest->y;
}
int Predmet::getW(){
	return dest->w;
}
int Predmet::getH(){
	return dest->h;
}
