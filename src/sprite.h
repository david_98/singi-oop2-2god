#ifndef SPRITE_H_INCLUDED
#define SPRITE_H_INCLUDED

#include <string>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "engine.h"

#include "drawable.h"
#include "movable.h"

#include "spritesheet.h"
#include "kolizijaInterface.h"
#include "metode.h"
#include "predmet.h"
#include "zidDesni.h"
#include "zidLevi.h"
#include "engine.h"
//#include "zadatak.h"

using namespace std;
class Engine;

class Sprite : public Drawable, public Movable, public KolizijaInterface {
public:
    enum State:short int {LEFT=1, RIGHT=2, UP=4, DOWN=8,
                     LEFT_STOP=3, UP_STOP=9,
                     RIGHT_STOP=5, DOWN_STOP=10};
private:
    short int state;
    SpriteSheet *sheet;
    SDL_Rect *spriteRect;
    int currentFrame;
    int frameCounter;
    int frameSkip;
    vector<Predmet*> predmeti;
    Engine* eng;
    ZidDesni* zidDesni;
    ZidLevi* zidLevi;
//    Zadatak* prikazaniZadatak;
public:
    Sprite(SpriteSheet *sheet, int, int, ZidDesni*, ZidLevi*, Engine*);
    int getFrameSkip();
    void setFrameSkip(int frameSkip);
    short int getState();
    void setState(short int state);
    virtual void draw(SDL_Renderer *renderer);
    virtual void move();
    virtual void move(int dx, int dy);
    void putInPredmeti(Predmet*);
    SDL_Rect* getSpriteRect();
    // neophodno radi interface-a za koliziju
    virtual int getX();
    virtual int getY();
    virtual int getW();
    virtual int getH();
};

#endif // SPRITE_H_INCLUDED
