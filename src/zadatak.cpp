/*
 * zadatak.cpp
 *
 *  Created on: Feb 1, 2020
 *      Author: david
 */

#include "zadatak.h"

Zadatak::Zadatak(SDL_Renderer *renderer, string zadatak, vector<int> rezultatiZadatka, int pozicijaOdgovora) {
	this->renderer = renderer;
	this->font = TTF_OpenFont("lazy.ttf", 25);
	this->color = {255, 255, 255, 255};
	this->textSurface = TTF_RenderText_Solid(font, zadatak.c_str(), color);
	this->text = SDL_CreateTextureFromSurface(renderer, textSurface);
	SDL_FreeSurface(textSurface);
	this->textRect.x = 278;
	this->textRect.y = 70;
	this->textRect.w = 192;
	this->textRect.h = 32;

	SDL_QueryTexture(text, NULL, NULL, &textRect.w, &textRect.h);

	this->rezultatiZadatka = rezultatiZadatka;
	this->pozicijaOdgovora = pozicijaOdgovora;
}

vector<int> Zadatak::getRezultatiZadatka(){
	return rezultatiZadatka;
}

int Zadatak::getPozicijaOdgovora(){
	return pozicijaOdgovora;
}

SDL_Texture* Zadatak::getTexture(){
	return text;
}

SDL_Rect& Zadatak::getRectangle(){
	return textRect;
}

void Zadatak::draw(SDL_Renderer* renderer){
	SDL_RenderCopy(renderer, text, NULL, &textRect);

}


Zadatak::~Zadatak() {
	SDL_DestroyTexture(text);
}

